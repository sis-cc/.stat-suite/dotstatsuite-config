# Config server

To ease configurations management and avoid to spread setup data over all repos, config server centralize all configuration resources used by other services, mainly webapps but also servers.

`config server` is like an internal private http bucket shared by internal services to perform members's webapp cutomisations.

3 resources are managed by `config`:

* `assets` : starts by `/assets`: Those resources are accessed only via `proxy` from external requests (unprotected resources)
* `configs`: starts by `/configs`: thoses resources are only accessed by requests internal (protected resources) to the cluster coming from other servers (`data-explorer`, `sfs`, ...). 
* `i18n`: starts by `/i18n`: Those resources are only accessed by requests internal (protected resources) to the cluster coming from other servers (`data-explorer`, `sfs`, ...). 

## Resources principles

* resources path always starts with '/'

* DE and DLM use this scheme: `/${member.id}/${appId}/${path}`

* `appId` is an ID defining an app (in the case of `webapp` it's value is defined in `params/default.js`)

* It's under client responsability (data-explorer`, `sfs`, ...) to format correctly resource urls.

* `tenant` object is defined in `/configs/tenants.json` 


## Resources Providers

`config` can get resources from 5 differents providers:

* file system
* google bucket provider
* minio bucket provider
* azure bucket provider
* aws bucket provider

## Docker Images

gitops pipeline pushes 1 image:

* siscc/dotstatsuite-config


## Gitlab

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config/


## Architecture

It's mainly composed of a nodeJS server and configuration data.

server responds to GET /configs/<path_to_resource>,  GET /assets/<path_to_resource>, GET /i18n/<path_to_resource>, and GET /healthcheck

It's a simple http static files server.

It's under user responsability to manage content.

## Usage

define those environment variables at server start:

* `SERVER_HOST`: service's hostname
* `SERVER_PORT`: service's port

### File System Provider

* `BUCKET_PROVIDER`="fs"
* `DATA_DIR`: absolute path to folder that contains [`configs`, `assets`, `i18n`]

### Google Bucket Provider

* BUCKET_PROVIDER="gke"
* I18N_BUCKET="siscc-i18n"
* ASSETS_BUCKET="siscc-assets"
* CONFIGS_BUCKET="siscc-configs"
* GKE_KEY_FILE="/home/user/main.json"
* GOOGLE_CLOUD_PROJECT="main-234215"

### Minio Bucket Provider

* BUCKET_PROVIDER="minio"
* MINIO_ENDPOINT="localhost"
* MINIO_PORT=9000
* MINIO_ACCESS_KEY="minioadmin"
* MINIO_SECRET_KEY="minioadmin"
* I18N_BUCKET="siscc-i18n"
* ASSETS_BUCKET="siscc-assets"
* CONFIGS_BUCKET="siscc-configs"

### Azure Bucket Provider

* BUCKET_PROVIDER="azure"
* ACCOUNT_NAME="your-account-name"
* ACCOUNT_KEY="your-account-key"
* SAS="your-shared-access-signature-token"
* CONTAINER_NAME="your-container-name"

### AWS Bucket Provider

* BUCKET_PROVIDER="aws"
* REGION="aws-location"
* BUCKET_NAME="s3-bucket-name"
* USE_PROXY= Server is behind a proxy [`"true"`, `"false"`]
* ACCESS_KEY_ID="your-access-key-id"
* SECRET_ACCESS_KEY="your-access-key"
* SESSION_TOKEN_KEY="your-session-token"

### Use within a container

ex with a File System Provider

```
$ docker run  -d --name config -e BUCKET_PROVIDER=fs -v /tmp:/opt/data -e DATA_DIR=/opt/data  -p 5007:80 siscc/dotstatsuite-config:latest
```

### Development

Clone repo

```
$ yarn
$ yarn start:srv
```

### Production mode:

```
$ yarn dist
$ yarn dist:run
...
```

### Health checks


```
$ curl http://localhost:5007/healthcheck
{
gitHash: "#develop",
startTime: "2019-03-26T08:10:07.391Z"
}
```


### Test


To execute tests, run:
```
$ yarn test
```

To check coverage:
```
$ yarn test --coverage
```



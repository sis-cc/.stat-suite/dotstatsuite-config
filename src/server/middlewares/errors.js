import debug from '../debug';

const error = (err, req, res, next) => {
  if (!err) return next();
  if(err.code !== 404) debug.error(err);
  const code = err.code || 500;
  return res.sendStatus(code);
};

module.exports = error;

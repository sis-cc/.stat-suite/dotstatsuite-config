import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";
import { mockClient } from 'aws-sdk-client-mock';
import awsProvider from '../awsProvider';
import { HTTPError } from '../../errors';

describe('awsProvider', () => {
  let s3Mock;
  let awsProviderVar;

  beforeEach(() => {
    const config = {
      bucketsProvider: {
        region: 'us-west-2',
        bucketName: 'testBucket',
        accessKeyId: 'testAccessKeyId',
        secretAccessKey: 'testSecretAccessKey',
        sessionToken: 'testSessionToken',
        proxy: 'false'
      }
    };

    awsProviderVar = awsProvider(config);
    s3Mock = mockClient(S3Client);
  });

  afterEach(() => {
    s3Mock.restore();
  });

  it('should download file from S3', async () => {
    const mockFileBody = 'file content';
    s3Mock.on(GetObjectCommand).resolves({
      Body: mockFileBody
    });

    const bucketname = 'testBucket';
    const awsBucketName = 'awsTestBucket';

    const bucket = awsProviderVar(bucketname);
    const result = await bucket.downloadFile(awsBucketName);
    
    expect(result).toEqual(mockFileBody);
  });

  it('should throw HTTPClient 404 error on no such key error', async () => {
    s3Mock.on(GetObjectCommand).rejects({
      Code: 'NoSuchKey'
    });

    const bucketname = 'testBucket';
    const awsBucketName = 'awsTestBucket';
    const id = 'testId';

    const bucket = awsProviderVar(bucketname);

    await expect(bucket.downloadFile(awsBucketName, id)).rejects.toEqual(new HTTPError(404));
  });

  it('should throw HTTPClient 404 error on no such bucket error', async () => {
    s3Mock.on(GetObjectCommand).rejects({
      Code: 'NoSuchBucket'
    });

    const bucketname = 'testBucket';
    const awsBucketName = 'awsTestBucket';
    const id = 'testId';

    const bucket = awsProviderVar(bucketname);

    await expect(bucket.downloadFile(awsBucketName, id)).rejects.toEqual(new HTTPError(404));
  });
});
const nock = require('nock');
const Provider = require('..');
const HTTP_FAKE_URI = 'http://FAKE_URI';

describe.skip('server | assetsProvider | http', () => {
  it('should get resource', async () => {
    const URL = '/URL';
    const file = 'Hello world!';
    nock(HTTP_FAKE_URI).get(`/assets${URL}`).reply(200, file);
    const provider = Provider({ buckets: { assets: HTTP_FAKE_URI } });
    const data = await provider.get(URL);
    expect(data.toString()).toEqual(file);
  });

  it('should not upload', async () => {
    const URL = '/URL';
    const provider = Provider({ buckets: { assets: HTTP_FAKE_URI } });
    expect(() => provider.upload(URL)).toThrow();
  });
});
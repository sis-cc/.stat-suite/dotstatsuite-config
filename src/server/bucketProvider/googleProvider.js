import { Storage } from '@google-cloud/storage';
import { slice } from 'ramda';

const downloadFile = bucket => id => {
  const file = bucket.file(slice(1, Infinity, id));
  return file.createReadStream();
};

export default config => {
  const options = { projectId: config.bucketsProvider.projectId};
  if (config.bucketsProvider.keyFilename) options.keyFilename = config.bucketsProvider.keyFilename;
  const storage = new Storage(options);

  const res = bucketName => {
    const bucket = storage.bucket(bucketName);
    return {
      name: 'gke',
      downloadFile: downloadFile(bucket),
    };
  };

  return res;
};

import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";
import { addProxyToClient } from 'aws-sdk-v3-proxy'
import { HTTPError } from '../errors';

const downloadFile = (s3Client, bucketname, awsBucketName) => async id => {
  return new Promise(async (resolve, reject) => {
    const filePath = bucketname + id;
    const command = new GetObjectCommand({
      Bucket: awsBucketName,
      Key: filePath
    });

    try {
      const item = await s3Client.send(command);
      resolve(item.Body);
    }
    catch(error){
      if (error.Code === 'NoSuchKey') return reject(new HTTPError(404));
      if (error.Code === 'NoSuchBucket') return reject(new HTTPError(404));
      return reject(error);
      
    }
  })
};

export default config => {
  let conf = {
    region: config.bucketsProvider.region,
    bucketName: config.bucketsProvider.bucketName,
  };

  // If the accessKeyId is not null, then add the credentails to the payload.
  // CLI will ignore blank values
  if (config.bucketsProvider.accessKeyId){
    conf.credentials = {
      accessKeyId: config.bucketsProvider.accessKeyId,
      secretAccessKey: config.bucketsProvider.secretAccessKey,
      sessionToken: config.bucketsProvider.sessionToken,
    }
  }

  // AWS CLI does not respect device proxy settings.
  // This will use the required package if proxy is require.
  const s3Client = config.bucketsProvider.proxy == "true"
    ? addProxyToClient(new S3Client(conf))
    : new S3Client(conf);

  return bucketname => ({
    name: 'aws',
    downloadFile: downloadFile(s3Client, bucketname, config.bucketsProvider.bucketName),
  });
};
import got from 'got';
import { initResources } from 'jeto';
import { Readable } from 'stream';
import initHttp from '../init/http';
import initRouter from '../init/router';

const tenants = {
  orox: {
    name: 'orox',
    spaces: {
      collect: {
        label: 'collect'
      },
    }
  }
};

const res = {
  orox: {
    id: 'orox',
    name: 'orox',
    spaces: {
      collect: {
        id: 'collect',
        label: 'collect'
      },
    }
  }
};


const initBuckets = ctx => {
  const bucketProvider = () => ({
    name: 'test',
    downloadFile : () => Promise.resolve(Readable.from(Buffer.from(JSON.stringify(tenants)))),
  });

  return ctx({ bucketProvider });
};

const initConfig = ctx => ctx({
  config: {
    buckets:{
      assets: 'assets',
      configs: 'configs',
      i18n: 'i18n',
    },
    isProduction: false,
    server: {
      host: 'localhost',
    },
  },
});

let CTX;

beforeAll(() => initResources([ initConfig, initBuckets, initRouter, initHttp]).then(ctx => { CTX = ctx }));

afterAll(() => { if(CTX) CTX().httpServer.close() });

describe('tenants', () => {
  it('should get tenants', async () => {
    const  { headers } = await got.get(`${CTX().httpServer.url}/configs/tenants.json`)
    expect(headers['content-type'].indexOf('application/json')).not.toEqual(-1);
    const  body = await got.get(`${CTX().httpServer.url}/configs/tenants.json`).json() ;
    expect(body).toEqual(res);
  });
});
